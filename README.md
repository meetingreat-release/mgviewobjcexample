# MGView Sample App #

MGView integration demo.

### 整合步驟 ###

* 將 MGView.framework 加入 General -> Embedded Binaries 與 General -> Linked Frameworks and Libraries 中

* 設定 Runpath Search Paths 為 @exectutable_path/Frameworks
 
* 將 Build Settings -> Build Options -> Embedded Content Contains Swift Code 設定成 YES


* 使用下面的 code present MGViewController (URL 請加上 channel=iOS)
```
#import <MGView/MGView-Swift.h>
...
    MGViewController *vc = [[MGViewController alloc] init];
    [vc setRoomUrl:@"http://server_url/cvc/r/RLKsxzpE8e8?channel=iOS"];
    [vc setDisplayName:@"MyName"];
    [self presentViewController:vc animated:YES completion:nil];
...
```