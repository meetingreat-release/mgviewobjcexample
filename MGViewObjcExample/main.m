//
//  main.m
//  MGViewObjcExample
//
//  Created by billyshieh on 2015/12/1.
//  Copyright © 2015年 Meetingreat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
