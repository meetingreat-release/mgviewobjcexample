//
//  AppDelegate.h
//  MGViewObjcExample
//
//  Created by billyshieh on 2015/12/1.
//  Copyright © 2015年 Meetingreat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

