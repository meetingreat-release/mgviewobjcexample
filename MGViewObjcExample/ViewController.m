//
//  ViewController.m
//  MGViewObjcExample
//
//  Created by billyshieh on 2015/12/1.
//  Copyright © 2015年 Meetingreat. All rights reserved.
//

#import "ViewController.h"
#import <MGView/MGView-Swift.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button setTitle:@"Test MGView" forState:UIControlStateNormal];
    [button sizeToFit];
    button.center = CGPointMake(160, 60);
    [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buttonPressed:(UIButton *)button {
    NSLog(@"Button Pressed");
    MGViewController *vc = [[MGViewController alloc] init];
//    [vc setRoomUrl:@"http://macbuntu.local/cvc/r/RLKsxzpE8e8?channel=iOS"];
    [vc setRoomUrl:@"https://mcu.meetingreat.com/cvc/r/9ntag9qnbp?channel=iOS"];
    [vc setDisplayName:@"MyName"];
//    [vc setVideoCodec:@"h264"]; // for low-end iOS device, using h264 hw codec

    NSString *(^handler)(NSString *message) = ^(NSString *message) {
        return [NSString stringWithFormat: @"%@ !!!", message];
    };
    [vc registerErrorHandler: handler];

    [self presentViewController:vc animated:YES completion:nil];

//    [self testAutoHangup:vc];
//    [self testAutoReconnect:vc];
}

- (void)testAutoHangup:(MGViewController *)vc {
    double delayInSeconds = 13.0;
    dispatch_time_t t = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(t, dispatch_get_main_queue(), ^(void){
        [vc requestHangup:NO];
    });
}

- (void)testAutoReconnect:(MGViewController *)vc {
    double delayInSeconds = 13.0;
    dispatch_time_t t = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(t, dispatch_get_main_queue(), ^(void){
        [vc requestReconnect];
    });
}

@end
